/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1_mineriadedatos;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.IOException;
import static java.nio.file.Files.list;
import static java.rmi.Naming.list;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

/**
 *
 * @author cesareduardo
 */
public class Practica1_MineriadeDatos {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) throws IOException {
        
        //Lista donde se guardarán todas las palabras del archivo pdf.
        List<String> palabras = new ArrayList<String>();
        //Variable que obtiene la ruta del archivo pdf.
        PdfReader pdfr = new PdfReader("F:\\Minería de Datos\\ch1.pdf");
        //Inicializamos la variable con la extreremos el texto completo.
        String ext = "";
        
        /*For para recorrer todas las páginas del archivp pdf.
        getNumberOfPages devuelve el número total de páginas del archivo*/
        for(int i = 1; i < pdfr.getNumberOfPages(); i++)
        {
            /*Guardamos todo el texto en la variable anteriormente inicializada.
            Le damos de parametros la ruta del archivo y la página de donde 
            queramos obtener el texto.*/
            ext = PdfTextExtractor.getTextFromPage(pdfr, i);
            /*Usamos StringTokenizer para separar las palabras o romper la cadena 
            del texto y ponerla en "tokens"*/
            StringTokenizer t = new StringTokenizer(ext);
            //Recorremos la variable t mientras tenga más palabras
            while(t.hasMoreTokens()){
                /*Añade las palabras de la página 1(i) a la lista.
                Cuando acaba vuelve a recorrer el ciclo y añade las palabras
                dela página 2y sí sucesivamente*/
                palabras.add(t.nextToken());
            }
        }

        /*Utilizamos Set ya que no permite valores repetidos y creamos un HashSet porque
        Set no puede trabajar con Arraylist directamente*/
        Set<String> repetida = new HashSet<String>(palabras);
        
        /*Recorremos el set y utilizamos el for each para recorrer todos los elementos de la variable
        repetida y cada valor de esta sera colocado en la variable palabra*/
        for (String palabra : repetida) {
        /*Por último imprimimos la palabra y ultilizamos el Collections.Frecuency para
            contar las palabras de repetidas de la colección de datos*/
        System.out.println(palabra + ": " + Collections.frequency(palabras, palabra));
}

    }
    
}
